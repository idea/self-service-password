{if $prehook_return and $display_prehook_error and $prehook_return > 0}
    <div class="result alert alert-warning">
    <p><i class="fa fa-fw fa-exclamation-triangle" aria-hidden="true"></i> {$prehook_output[0]}</p>
    </div>
{/if}
{if $posthook_return and $display_posthook_error and $posthook_return > 0}
    <div class="result alert alert-warning">
    <p><i class="fa fa-fw fa-exclamation-triangle" aria-hidden="true"></i> {$posthook_output[0]}</p>
    </div>
{/if}
{if $result !== "accountcreated"}
    <div class="help alert alert-warning"><p><i class="fa fa-fw fa-info-circle"></i> {$msg_createaccountbytokenhelp|unescape: "html" nofilter}</p></div>
    {if $result !== "tokenrequired" and $result !== "tokennotvalid"}
        {if $pwd_show_policy !== "never" and $pwd_show_policy_pos === 'above'}
            {include file="policy.tpl"}
        {/if}
        <div class="alert alert-info">
        <form action="#" method="post" class="form-horizontal">
            <input type="hidden" name="token" value="{$token}" />

            {foreach from=$create_account_extra_fields item=field}
                <div class="form-group">
                {* Should be translated *}
                    <label for="{$field["ldap_attribute"]}" class="col-sm-4 control-label">{$field["name"]}</label>
                    <div class="col-sm-8">
                        <div class="input-group">
                            <span class="input-group-addon"><i class="fa fa-fw fa-{$field["icon"]}"></i></span>
                            {if $field["type"] == "text"}
                    
                                <input type="text" autocomplete="new-password" name="{$field["ldap_attribute"]}" id="{$field["ldap_attribute"]}" class="form-control" placeholder="{$field["name"]}" />
                            {/if}
                            {if $field["type"] == "select"}
                                <select name="{$field["ldap_attribute"]}" id="{$field["ldap_attribute"]}" class="form-control">
                                    {foreach from=$field["options"] item=option}
                                      <option value="{$option["value"]}">{$option["name"]} {$option["vaue"]}</option>
                                    {/foreach}
                                </select>
                            {/if}
                        </div>
                    </div>
                </div>

            {/foreach}
            <div class="form-group">
                <label for="newpassword" class="col-sm-4 control-label">{$msg_password}</label>
                <div class="col-sm-8">
                    <div class="input-group">
                        <span class="input-group-addon"><i class="fa fa-fw fa-lock"></i></span>
                        <input type="password" autocomplete="new-password" name="newpassword" id="newpassword" class="form-control" placeholder="{$msg_newpassword}" />
                    </div>
                </div>
            </div>
            <div class="form-group">
                <label for="confirmpassword" class="col-sm-4 control-label">{$msg_confirmpassword}</label>
                <div class="col-sm-8">
                    <div class="input-group">
                        <span class="input-group-addon"><i class="fa fa-fw fa-lock"></i></span>
                        <input type="password" autocomplete="new-password" name="confirmpassword" id="confirmpassword" class="form-control" placeholder="{$msg_confirmpassword}" />
                    </div>
                </div>
            </div>
            <div class="form-group">
                <div class="col-sm-offset-4 col-sm-8">
                    <button type="submit" class="btn btn-success">
                        <i class="fa fa-fw fa-check-square-o"></i> {$msg_submit}
                    </button>
                </div>
            </div>
        </form>
        </div>
        {if $pwd_show_policy !== "never" and $pwd_show_policy_pos === 'below'}
            {include file="policy.tpl"}
        {/if}
    {/if}
{else}
    {if $create_account_redirect_url}
        <script>
            window.setTimeout(
                function(){
                    window.location.href = "{$create_account_redirect_url}";
                }
                , {$create_account_redirect_timeout}
            );
        </script>
    {/if}
{/if}
